<body class="g-sidenav-show   bg-gray-100">
	<div class="min-height-300 bg-primary position-absolute w-100"></div>
	<aside
		class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4"
		style="z-index: 0;" id="sidenav-main">
		<div class="sidenav-header">
			<i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
				aria-hidden="true" id="iconSidenav"></i>
			<a class="navbar-brand m-0" href="<?= base_url('Admin')?>">
				<img src="<?= base_url()?>assets/img/logo.png" class="navbar-brand-img h-100" alt="main_logo">
				<span class="ms-1 font-weight-bold">LAPOR Online</span>
			</a>
		</div>
		<hr class="horizontal dark mt-0">
		<div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('Admin')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="ni ni-tv-2 text-primary text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Dashboard</span>
					</a>
				</li>
				<li class="nav-item mt-3">
					<h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Tabel Masyarakat</h6>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Admin/masyarakat')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="fa fa-users mb-2 text-dark text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Data Masyarakat</span>
					</a>
				</li>
				<li class="nav-item mt-3">
					<h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Tabel Pengaduan</h6>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Admin/semua_pengaduan')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="ni ni-calendar-grid-58 text-dark text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Semua Pengaduan</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Admin/pengaduan_diproses')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="ni ni-calendar-grid-58 text-warning text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Proses Pengaduan</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Admin/pengaduan_selesai')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="ni ni-calendar-grid-58 text-success text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Pengaduan Selesai</span>
					</a>
				</li>
				<li class="nav-item mt-3">
					<h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Tabel Tanggapan</h6>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Admin/tanggapan')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="ni ni-calendar-grid-58 text-dark text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Semua Tanggapan</span>
					</a>
				</li>
				<li class="nav-item mt-3">
					<h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Halaman Lainnya</h6>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Admin/petugas')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="fa fa-user-plus ms-1 text-dark text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Data Petugas</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Profile')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="ni ni-single-02 text-dark text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Profile</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Home')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="ni ni-world-2 text-dark text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Halaman Utama</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="<?= base_url('Auth/logout')?>">
						<div
							class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
							<i class="ni ni-collection text-danger text-sm opacity-10"></i>
						</div>
						<span class="nav-link-text ms-1">Log Out</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="sidenav-footer mx-3 ">
			<div class="card card-plain shadow-none" id="sidenavCard">
				<img class="w-50 mx-auto" src="<?= base_url()?>assets_admin/img/illustrations/icon-documentation.svg"
					alt="sidebar_illustration">
				<div class="card-body text-center p-3 w-100 pt-0">
					<div class="docs-info">
						<h6 class="mb-0">Need help?</h6>
						<p class="text-xs font-weight-bold mb-0">Please check our docs</p>
					</div>
				</div>
			</div>
			<a href="https://www.creative-tim.com/learning-lab/bootstrap/license/argon-dashboard" target="_blank"
				class="btn btn-dark btn-sm w-100 mb-3">Documentation</a>
			<a class="btn btn-primary btn-sm mb-0 w-100"
				href="https://www.creative-tim.com/product/argon-dashboard-pro?ref=sidebarfree" type="button">Upgrade to
				pro</a>
		</div>
	</aside>
