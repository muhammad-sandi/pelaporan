		<!-- Header Content -->
		<div class="container-xxl py-5 bg-primary hero-header mb-5">
			<div class="container my-5 py-5 px-lg-5">
				<div class="row g-5 py-5">
					<div class="col-lg-6 text-center text-lg-start">
						<?php if (!empty($this->session->userdata('id_user'))) { ?>

						<h2 class="text-white mb-4 animated zoomIn">Halo, Selamat datang <?= $profile[0]['nama']?></h2>
						<p class="text-white pb-3 animated zoomIn">Lapor Online adalah layanan penyampaian semua aspirasi dan
							pengaduan masyarakat Indonesia melalui kanal berbasis website.<br>Ayo tulis laporanmu jika kamu
							sedang dalam kesulitan, kami akan selalu siap untuk membantumu!</p>
						<a href="<?= base_url('Profile')?>"
							class="btn btn-light py-sm-3 px-sm-5 rounded-pill me-3 animated slideInLeft">Halaman Profile</a>
						<?php if ($this->session->userdata('id_role') == 1 || $this->session->userdata('id_role') == 2) : ?>
						<a href="<?= base_url('Admin')?>"
							class="btn btn-outline-light py-sm-3 px-sm-5 rounded-pill animated slideInRight">Dashboard</a>
						<?php endif ; ?>
						<?php if ($this->session->userdata('id_role') == 3) : ?>
						<a href="<?= base_url('Auth/logout')?>"
							class="btn btn-outline-light py-sm-3 px-sm-5 rounded-pill animated slideInRight">Logout</a>
						<?php endif ; ?>

						<?php } else { ?>

						<h1 class="text-white mb-4 animated zoomIn">Pelayanan Aspirasi Online Masyarakat</h1>
						<p class="text-white pb-3 animated zoomIn">Lapor Online adalah layanan penyampaian semua aspirasi dan
							pengaduan masyarakat Indonesia melalui kanal berbasis website.<br>Ayo tulis laporanmu jika kamu
							sedang dalam kesulitan, kami akan selalu siap untuk membantumu!</p>
						<a href="<?= base_url('Auth')?>"
							class="btn btn-light py-sm-3 px-sm-5 rounded-pill me-3 animated slideInLeft">Masuk</a>
						<a href="<?= base_url('Auth/register')?>"
							class="btn btn-outline-light py-sm-3 px-sm-5 rounded-pill animated slideInRight">Daftar</a>

						<?php } ?>
					</div>
					<div class="col-lg-6 text-center text-lg-start">
						<img class="img-fluid animated zoomIn" style="margin-top: -100px;"
							src="<?= base_url()?>assets/img/home.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<!-- End Header Content -->

		<!-- Contact Start -->
		<div class="container-xxl py-5" id="laporan">
			<div class="container px-lg-5">
				<div class="row justify-content-center">

					<div class="col-lg-6 text-center text-lg-start">
						<img class="img-fluid mt-5 wow fadeInUp" src="<?= base_url()?>assets/img/form.gif" alt="">
					</div>

					<div class="col-lg-6">
						<div class="section-title position-relative text-center mb-5 pb-2 wow fadeInUp" data-wow-delay="0.1s">
							<h6 class="position-relative d-inline text-primary ps-4">Laporkan Aspirasi atau Masalah Anda</h6>
							<h2 class="mt-2">Sampaikan Laporan Anda</h2>
						</div>
						<div class="wow fadeInUp" data-wow-delay="0.3s">
							<form method="POST" action="<?= base_url('Home/isiLaporan'); ?>" enctype="multipart/form-data">
								<div class="row g-3">
									<div class="col-md-6">
										<div class="form-floating">
											<input type="text" class="form-control" id="judul" name="judul"
												placeholder="Judul Laporan" required>
											<label for="name">Judul Laporan</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-floating">
											<input type="datetime-local" class="form-control" id="tgl_kejadian"
												name="tgl_kejadian" placeholder="Tanggal Kejadian" required>
											<label for="email">Tangal Kejadian</label>
										</div>
									</div>
									<div class="col-12">
										<div class="form-floating">
											<input type="file" class="form-control" id="foto" name="foto">
											<label for="subject">Foto</label>
										</div>
									</div>
									<div class="col-12">
										<div class="form-floating">
											<select class="form-select" id="privasi" name="privasi"
												aria-label="Default select example" placeholder="Privasi Laporan" required>
												<option value="publik">Publik</option>
												<option value="rahasia">Rahasia</option>
											</select>
											<label for="message">Privasi Laporan</label>
										</div>
									</div>
									<div class="col-12">
										<div class="form-floating">
											<textarea class="form-control" placeholder="Isi Laporan" id="isi" name="isi"
												style="height: 150px" required></textarea>
											<label for="message">Isi Laporan</label>
										</div>
									</div>
									<div class="col-12">
										<?php if (!empty($this->session->userdata('id_user')) && $this->session->userdata('id_role') == 3) { ?>
										<button class="btn btn-primary w-100 py-3" type="submit">Kirim Laporan</button>
										<?php } else { ?>
										<button class="btn btn-primary w-100 py-3" type="submit" disabled>Kirim
											Laporan</button>
										<?php } ?>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- Contact End -->

		<!-- Newsletter Start -->
		<!-- <div class="container-xxl bg-primary newsletter my-5 wow fadeInUp" data-wow-delay="0.1s">
			<div class="container px-lg-5">
				<div class="row align-items-center" style="height: 250px;">
					<div class="col-12 col-md-6">
						<h3 class="text-white">Sampaikan Laporan Anda</h3>
						<small class="text-white">Diam elitr est dolore at sanctus nonumy.</small>
						<div class="position-relative w-100 mt-3">
							<input class="form-control border-0 rounded-pill w-100 ps-4 pe-5" type="text"
								placeholder="Judul Laporan" style="height: 48px;">
							
						</div>
						<div class="position-relative w-100 mt-3">
							<input class="form-control border-0 rounded-pill w-100 ps-4 pe-5" type="text"
								placeholder="Enter Your Email" style="height: 48px;">
						</div>
					</div>
					<div class="col-md-6 text-center mb-n5 d-none d-md-block">
						<img class="img-fluid mt-5" style="height: 250px;"
							src="<?= base_url()?>assets/img/newsletter.png">
					</div>
				</div>
			</div>
		</div> -->
		<!-- Newsletter End -->

		<!-- Service Start -->
		<div class="container-xxl py-5" id="cara-laporan">
			<div class="container px-lg-5">
				<div class="section-title position-relative text-center mb-5 pb-2 wow fadeInUp" data-wow-delay="0.1s">
					<h6 class="position-relative d-inline text-primary ps-4">Buat Laporan Anda</h6>
					<h2 class="mt-2">Cara Membuat Laporan</h2>
				</div>
				<div class="row g-4">
					<div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
						<div class="service-item d-flex flex-column justify-content-center text-center rounded">
							<div class="service-icon flex-shrink-0">
								<i class="fa fa-search fa-2x"></i>
							</div>
							<h5 class="mb-3">Mengunjungi Website</h5>
							<p>Mengunjungi website lapor online.</p>
							<a class="btn px-3 mt-auto mx-auto">Langkah ke-1</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
						<div class="service-item d-flex flex-column justify-content-center text-center rounded">
							<div class="service-icon flex-shrink-0">
								<i class="fa fa-user-plus fa-2x"></i>
							</div>
							<h5 class="mb-3">Mendaftar Akun</h5>
							<p>Pengunjung mendaftarkan akun dan melengkapi data diri.</p>
							<a class="btn px-3 mt-auto mx-auto">Langkah ke-2</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
						<div class="service-item d-flex flex-column justify-content-center text-center rounded">
							<div class="service-icon flex-shrink-0">
								<i class="fa fa-user fa-2x"></i>
							</div>
							<h5 class="mb-3">Login</h5>
							<p>Pengunjung login menggunakan akun yang telah dibuat.</p>
							<a class="btn px-3 mt-auto mx-auto">Langkah ke-3</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
						<div class="service-item d-flex flex-column justify-content-center text-center rounded">
							<div class="service-icon flex-shrink-0">
								<i class="fa fa-bullhorn fa-2x"></i>
							</div>
							<h5 class="mb-3">Membuat Laporan</h5>
							<p>Pengunjung membuat laporan dengan cara mengisi form pelaporan.</p>
							<a class="btn px-3 mt-auto mx-auto">Langkah ke-4</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
						<div class="service-item d-flex flex-column justify-content-center text-center rounded">
							<div class="service-icon flex-shrink-0">
								<i class="fa fa-cogs fa-2x"></i>
							</div>
							<h5 class="mb-3">Proses Verifikasi</h5>
							<p>Petugas melakukan proses verifikasi pelaporan.</p>
							<a class="btn px-3 mt-auto mx-auto">Langkah ke-5</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
						<div class="service-item d-flex flex-column justify-content-center text-center rounded">
							<div class="service-icon flex-shrink-0">
								<i class="fa fa-check fa-2x"></i>
							</div>
							<h5 class="mb-3">Laporan Selesai</h5>
							<p>Laporan telah selesai dibuat dan akan tampil di pelaporan publik jika privasi laporan yang
								dipilih adalah publik.</p>
							<a class="btn px-3 mt-auto mx-auto">Langkah ke-6</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Service End -->

		<!-- Pelaporan Publik Start -->
		<div id="laporan-publik" class="section-title position-relative text-center mt-5 mb-5 pb-2 wow fadeInUp"
			data-wow-delay="0.1s">
			<h6 class="position-relative d-inline text-primary ps-4">Dengarkan Kata Mereka</h6>
			<h2 class="mt-2">Pelaporan Publik</h2>
		</div>
		<div class="container-xxl bg-primary testimonial py-5 my-5 wow fadeInUp" data-wow-delay="0.1s">
			<div class="container py-5 px-lg-5">
				<div class="owl-carousel testimonial-carousel">
					<?php if (!empty($publik)) : ?>
					<?php foreach ($publik as $p) : ?>
					<div class="testimonial-item bg-transparent border rounded text-white p-4">
						<i class="fa fa-quote-left fa-2x mb-3"></i>
						<p><?= $p['isi_laporan']?></p>
						<div class="d-flex align-items-center">
							<img class="img-fluid flex-shrink-0 rounded-circle"
								src="<?= base_url('uploads/FileProfile/'.$p['foto']) ?>" style="width: 50px; height: 50px;">
							<div class="ps-3">
								<h6 class="text-white mb-1"><?= $p['nama']?></h6>
								<small><?= $p['username']?></small>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!-- Pelaporan Publik End -->


		<!-- Jumlah Laporan -->
		<div class="container-xxl py-5" id="jumlah-laporan">
			<div class="container px-lg-5">
				<div class="section-title position-relative text-center mb-5 pb-2 wow fadeInUp" data-wow-delay="0.1s">
					<h6 class="position-relative d-inline text-primary ps-4">Jumlah Laporan</h6>
					<h2 class="mt-2">Jumlah Laporan Sekarang</h2>
				</div>
				<div class="row g-4">
					<!-- <div class="count-wrapper wow fadeInUp">
						<div class="count-text"><?= $jumlahlaporan; ?></div>
					</div> -->
					<div class="counter wow fadeInUp isShown">
						<span id="count"><?= $jumlahlaporan; ?></span>
					</div>

				</div>
			</div>
		</div>
		<!-- Jumlah Laporan End -->

		<style>
			.counter {
				display: flex;
				align-items: center;
				justify-content: center;
				font-size: 4rem;
				font-weight: bold;
				color: #2124b1;
				padding: 1rem;
			}
		</style>

		<script>
			const counter = document.getElementById("count");
			let count = 0;
			const maxCount = <?= $jumlahlaporan ?> ;

			const observer = new IntersectionObserver((entries) => {
				if (entries[0].isIntersecting) {
					// elemen masuk ke viewport, mulai hitung counter
					setInterval(() => {
						if (count < maxCount) {
							count++;
							counter.innerText = count;
						}
					}, 100);
				} else {
					// elemen keluar dari viewport, stop hitung counter
					clearInterval();
				}
			});

			observer.observe(counter);

		</script>
