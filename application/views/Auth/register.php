<main class="main-content  mt-0">
	<section>
		<title>Lapor Online - Daftar</title>
		<div class="page-header min-vh-100">
			<div class="container">
				<div class="row">
					<div class="col-xl-5 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
						<div class="card card-plain">
							<div class="card-header pb-0 text-start">
								<h4 class="font-weight-bolder">Daftar Akun Lapor Online</h4>
								<p class="mb-0">Masukkan nama, username, dan password anda untuk mendaftar.</p>
							</div>
							<div class="card-body">
								<form method="post" action="<?= base_url('Auth/registration'); ?>">
									<div class="mb-3">
										<input type="text" name="nama" class="form-control form-control-lg"
											placeholder="Nama" required>
									</div>
									<div class="mb-3">
										<input type="text" name="username" class="form-control form-control-lg"
											placeholder="Username" required>
									</div>
									<div class="mb-3">
										<input type="password" name="password" class="form-control form-control-lg"
											placeholder="Password" required>
										<?= $this->session->flashdata('pesan') ?>
									</div>
									<div class="text-center">
										<button type="submit"
											class="btn btn-lg btn-primary btn-lg w-100 mt-4 mb-0">Daftar</button>
									</div>
								</form>
							</div>
							<div class="card-footer text-center pt-0 px-lg-2 px-1">
								<p class="mb-2 text-sm mx-auto">
									Sudah punya akun?
									<a href="<?= base_url('Auth'); ?>"
										class="text-primary text-gradient font-weight-bold">Masuk</a>
								</p>
								<p class="mb-4 text-sm mx-auto">
									Kembali ke
									<a href="<?= base_url('Home'); ?>"
										class="text-primary text-gradient font-weight-bold">Halaman Utama</a>
								</p>
							</div>
						</div>
					</div>
					<div
						class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column">
						<div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center overflow-hidden" style="background-image: url('<?= base_url()?>/assets/img/login.png');
          background-size: cover;">
							<span class="mask bg-gradient-primary opacity-6"></span>
							<h4 class="mt-5 text-white font-weight-bolder position-relative">"Ayo, Daftar Akun Lapor Online"</h4>
                			<p class="text-white position-relative">Daftar untuk membuat laporan, mengubah data diri, dan mendapatkan tanggapan oleh petugas.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
