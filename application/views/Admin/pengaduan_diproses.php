<main class="main-content position-relative border-radius-lg ">
	<!-- Navbar -->
	<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl " id="navbarBlur"
		data-scroll="false">
		<div class="container-fluid py-1 px-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
					<li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Admin</a>
					</li>
					<li class="breadcrumb-item text-sm text-white active" aria-current="page">Pengaduan Diproses</li>
				</ol>
				<h6 class="font-weight-bolder text-white mb-0">Proses Pengaduan</h6>
			</nav>
			<div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
				<div class="ms-md-auto pe-md-3 d-flex align-items-center">
					<form action="<?= base_url('Admin/pengaduan_diproses') ?>" method="post">
						<div class="input-group">
							<button type="submit" class="input-group-text text-body"><i class="fas fa-search"
									aria-hidden="true"></i></button>
							<input type="text" name="keyword" class="form-control" placeholder="Cari..."
								value="<?php echo $keyword ? $keyword : ''; ?>">
							<?php if (!empty($keyword)): ?>
							<button type="submit" class="input-group-text text-body" onclick="resetKeyword()"><i
									class="fas fa-times" aria-hidden="true"></i></button>
							<?php endif; ?>
						</div>
					</form>
				</div>
				<ul class="navbar-nav  justify-content-end">
					<li class="nav-item d-flex align-items-center">
						<a href="javascript:;" class="nav-link text-white font-weight-bold px-0">
							<i class="fa fa-user me-sm-1"></i>
							<span class="d-sm-inline d-none">Halo, <?= $profile[0]['nama']; ?></span>
						</a>
					</li>
					<li class="nav-item d-xl-none ps-3 d-flex align-items-center">
						<a href="javascript:;" class="nav-link text-white p-0" id="iconNavbarSidenav">
							<div class="sidenav-toggler-inner">
								<i class="sidenav-toggler-line bg-white"></i>
								<i class="sidenav-toggler-line bg-white"></i>
								<i class="sidenav-toggler-line bg-white"></i>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- End Navbar -->
	<div class="container-fluid py-4">
		<title>Proses Pengaduan</title>
		<div class="row">
			<div class="col-12">
				<div class="card mb-4">
					<div class="card-header d-flex justify-content-between pb-0">
						<!-- <h6>Pengaduan Yang Perlu Diproses</h6> -->
						<form action="<?php echo base_url('Admin/pengaduan_diproses');?>" method="post">
							<div class="form-group">
								<select name="bulan" class="form-control" onchange="this.form.submit()">
									<option value="">- Pilih Berdasarkan Bulan -</option>
									<option value="semua" <?php echo $bulan == 'semua' ? 'selected' : ''; ?>>Semua Bulan
									</option>
									<option value="01">Januari</option>
									<option value="02">Februari</option>
									<option value="03">Maret</option>
									<option value="04">April</option>
									<option value="05">Mei</option>
									<option value="06">Juni</option>
									<option value="07">Juli</option>
									<option value="08">Agustus</option>
									<option value="09">September</option>
									<option value="10">Oktober</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
							</div>
						</form>
						<div class="button">
							<?php if ($this->session->userdata('id_role') == 1) { ?>
							<?php if (!empty($keyword) ) { ?>
							<a class="btn btn-sm btn-primary"
								href="<?php echo base_url('Export/excel_pengaduan_diproses_search/'.$keyword.'/'.$bulan)?>"
								role="button"><i class="fa fa-cloud-download"></i>&ensp;Unduh Excel</a>
							<a class="btn btn-sm btn-primary" target="_blank"
								href="<?php echo base_url('Export/pdf_pengaduan_diproses_search/'.$keyword.'/'.$bulan)?>"
								role="button"><i class="fa fa-cloud-download"></i>&ensp;Unduh PDF</a>

							<?php } else { ?>

							<a class="btn btn-sm btn-primary"
								href="<?php echo base_url('Export/excel_pengaduan_diproses/'.$bulan)?>" role="button"><i
									class="fa fa-cloud-download"></i>&ensp;Unduh Excel</a>

							<a class="btn btn-sm btn-primary" target="_blank"
								href="<?php echo base_url('Export/pdf_pengaduan_diproses/'.$bulan)?>" role="button"><i
									class="fa fa-cloud-download"></i>&ensp;Unduh PDF</a>
							<?php } ?>
							<?php } ?>
						</div>
					</div>
					<div class="card-body px-0 pt-0 pb-2">
						<div class="table-responsive p-0">
							<table class="table align-items-center mb-0">
								<thead>
									<tr>
										<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Pelapor</th>
										<th
											class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
											Isi Laporan</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Foto</th>
										<!-- <th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Tgl Kejadian</th> -->
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Tgl Pelaporan</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Status</th>
										<th class="text-secondary opacity-7"></th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($pengaduan)) : ?>
									<?php foreach ($pengaduan as $p) : ?>
									<tr>
										<td>
											<div class="d-flex px-2 py-1">
												<div>
													<img src="<?= base_url('uploads/FileProfile/'.$p['foto']) ?>"
														class="avatar avatar-sm me-3" alt="user1">
												</div>
												<div class="d-flex flex-column justify-content-center">
													<h6 class="mb-0 text-sm"><?= $p['nama']?></h6>
													<p class="text-xs text-secondary mb-0"><?= $p['username']?></p>
												</div>
											</div>
										</td>

										<td>
											<p class="text-xs font-weight-bold mb-0"><?= $p['judul_laporan']?></p>
											<p class="text-xs text-secondary mb-0"><?= $p['isi_laporan']?></p>
										</td>

										<td class="align-middle text-center">
											<div>
												<img src="<?= base_url('uploads/FileLaporan/'.$p['foto_laporan']) ?>"
													class="avatar" alt="foto_laporan">
											</div>
										</td>

										<!-- <td class="align-middle text-center">
											<span class="text-secondary text-xs font-weight-bold"><?= $p['tgl_kejadian']?></span>
										</td> -->

										<td class="align-middle text-center">
											<span
												class="text-secondary text-xs font-weight-bold"><?= $p['tgl_pengaduan']?></span>
										</td>

										<td class="align-middle text-center text-sm">
											<span class="badge badge-sm bg-gradient-warning"><?= $p['status']?></span>
										</td>

										<td class="align-middle">
											<a href="#" class="text-success font-weight-bold text-xs"
												data-bs-toggle="modal" data-bs-target="#modal-pengaduan<?= $p['id']?>"
												target="_blank">
												<i class="fa fa-check"></i>
											</a>

											<a href="<?= base_url('Admin/tolakPengaduan/'.$p['id']) ?>"
												onclick="return confirm(`Apakah anda yakin?`)"
												class="text-danger font-weight-bold text-xs ms-3" data-toggle="tooltip"
												data-original-title="Edit user">
												<i class="fa fa-trash"></i>
											</a>
										</td>

									</tr>
									<?php endforeach; ?>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<?php if (!empty($pagination)) : ?>
						<div class="pagination-links d-flex justify-content-end me-3 mb-2">
							<?= $pagination ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>


		<!-- Modal -->
		<?php if (!empty($pengaduan)) : ?>
		<?php foreach ($pengaduan as $p) : ?>
		<div class="modal fade" id="modal-pengaduan<?= $p['id']?>" tabindex="-1">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content text-center">
					<div class="modal-body" style="padding: 30px;">
						<!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
						<h2 class="mb-3 text-start" style="font-weight: 800; font-size: 24px;">
							Tanggapi Laporan</h2>
						<form class="text-start" method="post" action="<?= base_url('Admin/terimaPengaduan'); ?>"
							enctype="multipart/form-data">
							<input type="hidden" name="id_pengaduan" value="<?= $p['id']?>">
							<input type="hidden" name="id_petugas" value="<?= $this->session->userdata('id_petugas')?>">
							<div class="form-floating">
								<textarea class="form-control" placeholder="Isi Laporan" id="tanggapan" name="tanggapan"
									style="height: 150px" required></textarea>
								<label for="message" style="font-weight: 550;">Isi Tanggapan</label>
							</div>
							<div class="d-flex justify-content-around">
								<button type="submit" class="btn btn-primary rounded-pill w-65 mt-4">Simpan</button>
								<button type="button" data-bs-dismiss="modal" aria-label="Close"
									class="btn btn-secondary rounded-pill w-25 mt-4">Batal</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
		<?php endif; ?>
		<!-- End Modal -->

		<style>
			.pagination-links {
				display: flex;
				justify-content: flex-end;
				margin-top: 1em;
			}

			.pagination-links a,
			.pagination-links strong {
				background-color: #fff;
				border: 1px solid #ddd;
				border-radius: 4px;
				color: #333;
				display: inline-block;
				margin-left: 5px;
				padding: 6px 12px;
				text-align: center;
				text-decoration: none;
				transition: all 0.3s ease;
			}

			.pagination-links a:hover,
			.pagination-links strong {
				background-color: #5e72e4;
				border-color: #5e72e4;
				color: #fff;
				transition: all 0.3s ease;
			}

		</style>

		<script>
			function resetKeyword() {
				document.querySelector('input[name="keyword"]').value = 'semua';
				document.querySelector('form').submit();
			}

		</script>
