<main class="main-content position-relative border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl " id="navbarBlur"
		data-scroll="false">
		<div class="container-fluid py-1 px-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
					<li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Admin</a>
					</li>
					<li class="breadcrumb-item text-sm text-white active" aria-current="page">Dashboard</li>
				</ol>
				<h6 class="font-weight-bolder text-white mb-0">Dashboard</h6>
			</nav>
			<div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
				<div class="ms-md-auto pe-md-3 d-flex align-items-center">
					<div class="input-group">
						<span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
						<input type="text" class="form-control" placeholder="Cari...">
					</div>
				</div>
				<ul class="navbar-nav  justify-content-end">
					<li class="nav-item d-flex align-items-center">
						<a href="javascript:;" class="nav-link text-white font-weight-bold px-0">
							<i class="fa fa-user me-sm-1"></i>
							<span class="d-sm-inline d-none">Halo, <?= $profile[0]['nama']; ?></span>
						</a>
					</li>
					<li class="nav-item d-xl-none ps-3 d-flex align-items-center">
						<a href="javascript:;" class="nav-link text-white p-0" id="iconNavbarSidenav">
							<div class="sidenav-toggler-inner">
								<i class="sidenav-toggler-line bg-white"></i>
								<i class="sidenav-toggler-line bg-white"></i>
								<i class="sidenav-toggler-line bg-white"></i>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
	<title>Dashboard</title>
      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-sm mb-0 text-uppercase font-weight-bold">Semua Pengaduan</p>
                    <h5 class="font-weight-bolder">
                      <?= $semua_pengaduan; ?>
                    </h5>
                    
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                    <i class="ni ni-notification-70 text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
						<div class="mb-0 d-flex justify-content-center">
										<a href="<?= base_url('Admin/semua_pengaduan');?>" class="btn btn-primary rounded-pill w-85">Lihat Tabel</a>
                    </div>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-sm mb-0 text-uppercase font-weight-bold">Pengaduan Diproses</p>
                    <h5 class="font-weight-bolder">
                      <?= $pengaduan_diproses; ?>
                    </h5>
                    
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                    <i class="ni ni-notification-70 text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
						<div class="mb-0 d-flex justify-content-center">
										<a href="<?= base_url('Admin/pengaduan_diproses');?>" class="btn btn-primary rounded-pill w-85">Lihat Tabel</a>
                    </div>
          </div>
        </div>
				<div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-sm mb-0 text-uppercase font-weight-bold">Pengaduan Selesai</p>
                    <h5 class="font-weight-bolder">
                      <?= $pengaduan_selesai; ?>
                    </h5>
                    
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                    <i class="ni ni-notification-70 text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
						<div class="mb-0 d-flex justify-content-center">
										<a href="<?= base_url('Admin/pengaduan_selesai');?>" class="btn btn-primary rounded-pill w-85">Lihat Tabel</a>
                    </div>
          </div>
        </div>
				<div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-sm mb-0 text-uppercase font-weight-bold">Data Masyarakat</p>
                    <h5 class="font-weight-bolder">
                      <?= $masyarakat; ?>
                    </h5>
                    
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                    <i class="ni ni-single-02 text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
						<div class="mb-0 d-flex justify-content-center">
										<a href="<?= base_url('Admin/masyarakat');?>" class="btn btn-primary rounded-pill w-85">Lihat Tabel</a>
                    </div>
          </div>
        </div>
      </div>
      
      <div class="row mt-4">
        <div class="col-lg-7 mb-lg-0 mb-4">
				<div class="row">
			<div class="col-12">
				<div class="card mb-4">
					<div class="card-header d-flex justify-content-between pb-0">
						<h6>Pengaduan Terbaru</h6>
					</div>
					<div class="card-body px-0 pt-0 pb-2">
						<div class="table-responsive p-0">
							<table class="table align-items-center mb-0">
								<thead>
									<tr>
										<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Pelapor</th>
										<th
											class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
											Isi Laporan</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Foto</th>
										<!-- <th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Tgl Kejadian</th> -->
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Tgl Pelaporan</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Status</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($pengaduan)) : ?>
									<?php foreach ($pengaduan as $p) : ?>
									<tr>
										<td>
											<div class="d-flex px-2 py-1">
												<div>
													<img src="<?= base_url('uploads/FileProfile/'.$p['foto']) ?>"
														class="avatar avatar-sm me-3" alt="user1">
												</div>
												<div class="d-flex flex-column justify-content-center">
													<h6 class="mb-0 text-sm"><?= $p['nama']?></h6>
													<p class="text-xs text-secondary mb-0"><?= $p['username']?></p>
												</div>
											</div>
										</td>

										<td>
											<p class="text-xs font-weight-bold mb-0"><?= $p['judul_laporan']?></p>
											<p class="text-xs text-secondary mb-0"><?= $p['isi_laporan']?></p>
										</td>

										<td class="align-middle text-center">
											<div>
												<img src="<?= base_url('uploads/FileLaporan/'.$p['foto_laporan']) ?>"
													class="avatar" alt="foto_laporan">
											</div>
										</td>

										<!-- <td class="align-middle text-center">
											<span class="text-secondary text-xs font-weight-bold"><?= $p['tgl_kejadian']?></span>
										</td> -->

										<td class="align-middle text-center">
											<span
												class="text-secondary text-xs font-weight-bold"><?= $p['tgl_pengaduan']?></span>
										</td>

										<td class="align-middle text-center text-sm">
											<?php if ($p['status'] == 'proses') { ?>
											<span class="badge badge-sm bg-gradient-warning"><?= $p['status']?></span>
											<?php } else { ?>
											<span class="badge badge-sm bg-gradient-success"><?= $p['status']?></span>
											<?php } ?>
										</td>

									</tr>
									<?php endforeach; ?>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<?php if (!empty($this->pagination->create_links())) : ?>
						<div class="pagination-links d-flex justify-content-end me-3 mb-2">
							<?php echo $this->pagination->create_links(); ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
        </div>
        <div class="col-lg-5">
<div class="card card-carousel overflow-hidden h-100 p-0">
<div id="carouselExampleCaptions" class="carousel slide h-100" data-bs-ride="carousel">
<div class="carousel-inner border-radius-lg h-100">
<div class="carousel-item h-100 carousel-item-next carousel-item-start" style="background-image: url('<?= base_url()?>assets_admin/img/carousel-1.jpg');
      background-size: cover;">
<div class="carousel-caption d-none d-md-block bottom-0 text-start start-0 ms-5">
<div class="icon icon-shape icon-sm bg-white text-center border-radius-md mb-3">
<i class="ni ni-camera-compact text-dark opacity-10"></i>
</div>
<h5 class="text-white mb-1">Get started with Argon</h5>
<p>There’s nothing I really wanted to do in life that I wasn’t able to get good at.</p>
</div>
</div>
<div class="carousel-item h-100" style="background-image: url('<?= base_url()?>assets_admin/img/carousel-2.jpg');
      background-size: cover;">
<div class="carousel-caption d-none d-md-block bottom-0 text-start start-0 ms-5">
<div class="icon icon-shape icon-sm bg-white text-center border-radius-md mb-3">
<i class="ni ni-bulb-61 text-dark opacity-10"></i>
</div>
<h5 class="text-white mb-1">Faster way to create web pages</h5>
<p>That’s my skill. I’m not really specifically talented at anything except for the ability to learn.</p>
 </div>
</div>
<div class="carousel-item h-100 active carousel-item-start" style="background-image: url('<?= base_url()?>assets_admin/img/carousel-3.jpg');
      background-size: cover;">
<div class="carousel-caption d-none d-md-block bottom-0 text-start start-0 ms-5">
<div class="icon icon-shape icon-sm bg-white text-center border-radius-md mb-3">
<i class="ni ni-trophy text-dark opacity-10"></i>
</div>
<h5 class="text-white mb-1">Share with us your design tips!</h5>
<p>Don’t be afraid to be wrong because you can’t learn anything from a compliment.</p>
</div>
</div>
</div>
<button class="carousel-control-prev w-5 me-3" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
<span class="carousel-control-prev-icon" aria-hidden="true"></span>
<span class="visually-hidden">Previous</span>
</button>
<button class="carousel-control-next w-5 me-3" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
<span class="carousel-control-next-icon" aria-hidden="true"></span>
<span class="visually-hidden">Next</span>
</button>
</div>
</div>
</div>
      </div>
