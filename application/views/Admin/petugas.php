<main class="main-content position-relative border-radius-lg ">
	<!-- Navbar -->
	<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl " id="navbarBlur"
		data-scroll="false">
		<div class="container-fluid py-1 px-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
					<li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Admin</a>
					</li>
					<li class="breadcrumb-item text-sm text-white active" aria-current="page">Data Petugas</li>
				</ol>
				<h6 class="font-weight-bolder text-white mb-0">Data Petugas</h6>
			</nav>
			<div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
				<div class="ms-md-auto pe-md-3 d-flex align-items-center">
					<form action="<?= base_url('Admin/petugas') ?>" method="post">
						<div class="input-group">
							<button type="submit" class="input-group-text text-body"><i class="fas fa-search"
									aria-hidden="true"></i></button>
							<input type="text" name="keyword" class="form-control" placeholder="Cari..."
								value="<?php echo $keyword ? $keyword : ''; ?>">
							<?php if (!empty($keyword)): ?>
							<button type="submit" class="input-group-text text-body" onclick="resetKeyword()"><i
									class="fas fa-times" aria-hidden="true"></i></button>
							<?php endif; ?>
						</div>
					</form>
				</div>
				<ul class="navbar-nav  justify-content-end">
					<li class="nav-item d-flex align-items-center">
						<a href="javascript:;" class="nav-link text-white font-weight-bold px-0">
							<i class="fa fa-user me-sm-1"></i>
							<span class="d-sm-inline d-none">Halo, <?= $profile[0]['nama']; ?></span>
						</a>
					</li>
					<li class="nav-item d-xl-none ps-3 d-flex align-items-center">
						<a href="javascript:;" class="nav-link text-white p-0" id="iconNavbarSidenav">
							<div class="sidenav-toggler-inner">
								<i class="sidenav-toggler-line bg-white"></i>
								<i class="sidenav-toggler-line bg-white"></i>
								<i class="sidenav-toggler-line bg-white"></i>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- End Navbar -->
	<div class="container-fluid py-4">
		<title>Data Petugas</title>
		<div class="row">
			<div class="col-12">
				<?= $this->session->flashdata('pesan') ?>
				<div class="card mb-4">
					<div class="card-header d-flex justify-content-between pb-0">
						<h6>Data Petugas</h6>
						<div class="button">
							<?php if ($this->session->userdata('id_role') == 1) { ?>
							<a class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#modal-petugas"
								target="_blank" role="button"><i class="fa fa-user-plus"></i>&ensp;Tambah Petugas</a>

							<a class="btn btn-sm btn-primary" href="<?php echo base_url('Export/excel_petugas/'.$keyword)?>"
								role="button"><i class="fa fa-cloud-download"></i>&ensp;Unduh Excel</a>

							<a class="btn btn-sm btn-primary" target="_blank"
								href="<?php echo base_url('Export/pdf_petugas/'.$keyword)?>" role="button"><i
									class="fa fa-cloud-download"></i>&ensp;Unduh PDF</a>
							<?php } ?>
						</div>
					</div>
					<div class="card-body px-0 pt-0 pb-2">
						<div class="table-responsive p-0">
							<table class="table align-items-center mb-0">
								<thead>
									<tr>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											No</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Foto Profil</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Nama</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Username</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											No. Telp</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Role Petugas</th>
										<th
											class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
											Status</th>
										<?php if ($this->session->userdata('id_role') == 1) { ?>
										<th class="text-secondary opacity-7"></th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($petugas)) : ?>
									<?php $no = 1; foreach ($petugas as $p) { ?>
									<tr>
										<td class="align-middle text-center">
											<span class="text-secondary text-xs font-weight-bold"><?= $no ?></span>
										</td>

										<td class="align-middle text-center">
											<div>
												<img src="<?= base_url('uploads/FileProfile/'.$p['foto']) ?>"
													class="avatar avatar-sm" alt="user1">
											</div>
										</td>

										<td class="align-middle text-center">
											<p class="text-xs font-weight-bold mb-0"><?= $p['nama']?></p>
										</td>

										<td class="align-middle text-center">
											<p class="text-xs font-weight-bold mb-0"><?= $p['username']?></p>
										</td>

										<td class="align-middle text-center">
											<span
												class="text-secondary text-xs font-weight-bold"><?= $p['telp']?></span>
										</td>

										<td class="align-middle text-center">
											<span
												class="text-secondary text-xs font-weight-bold"><?= $p['role']?></span>
										</td>
										<td class="align-middle text-center">
											<span
												class="text-secondary text-xs font-weight-bold"><?= $p['status_user']?></span>
										</td>
										<?php if ($this->session->userdata('id_role') == 1) { ?>
										<td class="align-middle">
											<?php if ($p['status_user'] == 'proses') { ?>
											<a href="<?= base_url('Admin/aktivasiUser/'.$p['id_user']) ?>"
												onclick="return confirm(`Apakah anda yakin?`)"
												class="text-success font-weight-bold text-xs">
												<i class="fa fa-check"></i>
											</a>

											<a href="<?= base_url('Admin/hapusUser/'.$p['id_user']) ?>"
												onclick="return confirm(`Apakah anda yakin?`)"
												class="text-danger font-weight-bold text-xs ms-3" data-toggle="tooltip"
												data-original-title="Edit user">
												<i class="fa fa-trash"></i>
											</a>
											<?php } ?>
										</td>
										<?php } ?>
									</tr>
									<?php $no++; } ?>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<?php if (!empty($pagination)) : ?>
						<div class="pagination-links d-flex justify-content-end me-3 mb-2">
							<?= $pagination ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>


		<!-- Modal -->

		<div class="modal fade" id="modal-petugas" tabindex="-1">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content text-center">
					<div class="modal-body" style="padding: 30px;">
						<!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
						<h2 class="mb-3 text-start" style="font-weight: 800; font-size: 24px;">
							Tambah Petugas</h2>
						<form class="text-start" method="post" action="<?= base_url('Admin/tambahPetugas'); ?>"
							enctype="multipart/form-data">
							<div class="form-floating mb-2">
								<input type="text" name="nama" class="form-control form-control-lg" placeholder="Nama"
									required>
								<label for="message" style="font-weight: 550;">Nama</label>
							</div>

							<div class="form-floating mb-2">
								<input type="text" name="telp" class="form-control form-control-lg" placeholder="Telp"
									required>
								<label for="message" style="font-weight: 550;">No. Telp</label>
							</div>

							<div class="form-floating mb-2">
								<input type="text" name="username" class="form-control form-control-lg"
									placeholder="Username" required>
								<label for="message" style="font-weight: 550;">Username</label>
							</div>

							<div class="form-floating">
								<input type="text" name="password" class="form-control form-control-lg"
									placeholder="Password" required>
								<label for="message" style="font-weight: 550;">Password</label>
							</div>

							<?= $this->session->flashdata('pesan') ?>

							<div class="d-flex justify-content-around">
								<button type="submit" class="btn btn-primary rounded-pill w-65 mt-4">Simpan</button>
								<button type="button" data-bs-dismiss="modal" aria-label="Close"
									class="btn btn-secondary rounded-pill w-25 mt-4">Batal</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- End Modal -->

		<style>
			.pagination-links {
				display: flex;
				justify-content: flex-end;
				margin-top: 1em;
			}

			.pagination-links a,
			.pagination-links strong {
				background-color: #fff;
				border: 1px solid #ddd;
				border-radius: 4px;
				color: #333;
				display: inline-block;
				margin-left: 5px;
				padding: 6px 12px;
				text-align: center;
				text-decoration: none;
				transition: all 0.3s ease;
			}

			.pagination-links a:hover,
			.pagination-links strong {
				background-color: #5e72e4;
				border-color: #5e72e4;
				color: #fff;
				transition: all 0.3s ease;
			}

		</style>

		<script>
			function resetKeyword() {
				document.querySelector('input[name="keyword"]').value = 'semua';
				document.querySelector('form').submit();
			}

		</script>
