<?php
//
// header("Content-type: application/octet-stream");



header("Content-Type: application/xls");


header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<h5>Data Pengaduan Diproses</h5>
<table id="datatable" class="table table-bordered" style="cursor:pointer;" border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Username</th>
			<th>Judul Laporan</th>
			<th>Isi Laporan</th>
			<th>Tgl Pengaduan</th>
			<th>Tgl Kejadian</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
        foreach ($data as $row) { ?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $row['nama']; ?></td>
			<td><?= $row['username']; ?></td>
			<td><?= $row['judul_laporan']; ?></td>
			<td><?= $row['isi_laporan']; ?></td>
			<td><?= $row['tgl_pengaduan']; ?></td>
			<td><?= $row['tgl_kejadian']; ?></td>
			<td><?= $row['status']; ?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>
