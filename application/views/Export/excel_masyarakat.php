<?php
//
// header("Content-type: application/octet-stream");



header("Content-Type: application/xls");


header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<h5>Data Masyarakat</h5>
<table id="datatable" class="table table-bordered" style="cursor:pointer;" border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Username</th>
			<th>NIK</th>
			<th>No. Telp</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
        foreach ($data as $row) { ?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $row['nama']; ?></td>
			<td><?= $row['username']; ?></td>
			<td style="mso-number-format:\@;"><?= $row['nik']; ?></td>
			<td style="mso-number-format:\@;"><?= $row['telp']; ?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>
