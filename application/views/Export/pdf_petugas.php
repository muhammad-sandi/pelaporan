<!DOCTYPE html>
<html>

<head>
	<title>Daftar Masyarakat</title>
	<style>
		body {
			font-family: 'Montserrat', sans-serif;
			font-size: 14px;
			line-height: 1.5;
			color: #333;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6 {
			margin-top: 0;
			font-weight: 700;
			line-height: 1.2;
			color: #333;
		}

		h2 {
			font-size: 24px;
			margin-bottom: 20px;
			text-align: center;
			text-transform: uppercase;
			letter-spacing: 1px;
			color: #1a1a1a;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			margin-bottom: 20px;
		}

		table thead th {
			text-align: center;
			padding: 10px;
			border: 1px solid #ddd;
			background-color: #f9f9f9;
			color: #666;
			text-transform: uppercase;
			font-size: 12px;
			letter-spacing: 1px;
		}

		table tbody td {
			text-align: center;
			padding: 10px;
			border: 1px solid #ddd;
			font-size: 14px;
			color: #333;
		}

	</style>
</head>

<body>
	<h2>Daftar Petugas</h2>
	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Username</th>
				<th>No. Telp</th>
				<th>Role Petugas</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1; foreach ($data as $row) { ?>
			<tr>
				<td><?= $no; ?></td>
				<td><?= $row['nama']; ?></td>
				<td><?= $row['username']; ?></td>
				<td style="mso-number-format:\@;"><?= $row['telp']; ?></td>
				<td><?= $row['role']; ?></td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>
</body>

</html>
