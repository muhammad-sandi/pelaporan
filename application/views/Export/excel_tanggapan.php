<?php
//
// header("Content-type: application/octet-stream");



header("Content-Type: application/xls");


header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<h5>Semua Tanggapan</h5>
<table id="datatable" class="table table-bordered" style="cursor:pointer;" border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Petugas</th>
			<th>Username Petugas</th>
			<th>Tanggapan</th>
			<th>Judul Laporan Yang Ditanggapi</th>
			<th>Isi Laporan Yang Ditanggapi</th>
			<th>Tgl Pelaporan</th>
			<th>Tgl Tanggapan</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
        foreach ($data as $row) { ?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $row['nama']; ?></td>
			<td><?= $row['username']; ?></td>
			<td><?= $row['tanggapan']; ?></td>
			<td><?= $row['judul_laporan']; ?></td>
			<td><?= $row['isi_laporan']; ?></td>
			<td><?= $row['tgl_pengaduan']; ?></td>
			<td><?= $row['tgl_tanggapan']; ?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>
