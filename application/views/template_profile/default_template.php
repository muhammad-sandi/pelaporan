<!-- <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr"> -->

<!-- BEGIN: Head-->
<?= $page['head'] ?>
<!-- END: Head-->
<!-- <link rel="stylesheet" href="<?= base_url() ?>app-assets/css/dash.css"> -->
<!-- BEGIN: Body-->

<!-- <body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col=""> -->

<!-- BEGIN: Content -->
<?= $content; ?>
<!-- END: Content-->
<!-- BEGIN: Main JS-->
<?= $page['main_js'] ?>
<!-- END: Main JS-->
<!-- </body> -->
<!-- END: Body-->

<!-- </html> -->
