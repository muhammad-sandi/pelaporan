<body class="g-sidenav-show bg-gray-100">
	<title>Profile</title>
	<div class="position-absolute w-100 min-height-300 top-0"
		style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/profile-layout-header.jpg'); background-position-y: 50%;">
		<span class="mask bg-primary opacity-6"></span>
	</div>
	<div class="main-content position-relative max-height-vh-100 h-100">
		<div class="card shadow-lg mx-4 card-profile-bottom">
			<div class="card-body p-3">
				<div class="row gx-4">
					<div class="col-auto">
						<div class="avatar avatar-xl position-relative">
							<img src="<?= base_url('uploads/FileProfile/'.$profile[0]['foto']) ?>" alt="profile_image"
								class="w-100 border-radius-lg shadow-sm">
						</div>
					</div>
					<div class="col-auto my-auto">
						<div class="h-100">
							<h5 class="mb-1">
								<?= $profile[0]['nama']?>
							</h5>
							<p class="mb-0 font-weight-bold text-sm">
								<?= $profile[0]['username']?>
							</p>
						</div>
					</div>
					<div class="col-lg-auto col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
						<div class="nav-wrapper position-relative end-0 d-flex">
							<ul class="nav nav-pills nav-fill p-1" role="tablist">
								<li class="nav-item">
									<a class="btn btn-primary text-white nav-link mb-0 active" href="#"
										data-bs-toggle="modal" data-bs-target="#modal-profile" target="_blank">
										<i class="ni ni-single-02"></i>
										<span class="ms-2">Ubah Foto Profil</span>
									</a>
								</li>
							</ul>
							<?php if ($this->session->userdata('id_role') == 3 ) {?>
							<ul class="nav nav-pills nav-fill p-1" role="tablist">
								<li class="nav-item">
									<a class="btn btn-primary text-white nav-link mb-0 active" href="#"
										data-bs-toggle="modal" data-bs-target="#modal-laporan" target="_blank">
										<i class="ni ni-calendar-grid-58"></i>
										<span class="ms-2">Laporan Saya</span>
									</a>
								</li>
							</ul>
							<?php } ?>
							<ul class="nav nav-pills nav-fill p-1" role="tablist">
								<li class="nav-item">
									<a class="btn btn-primary text-white nav-link mb-0 active"
										href="<?= base_url('Home')?>">
										<i class="ni ni-curved-next"></i>
										<span class="ms-2">Halaman Utama</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid py-4">
			<div class="row">
				<div class="col-md-12">
					<form method="POST" action="<?= base_url('Profile/updateUser'); ?>" enctype="multipart/form-data">
						<input type="hidden" name="id_user" value="<?= $profile[0]['id']?>">
						<div class="card">
							<div class="card-header pb-0">
								<div class="d-flex align-items-center">
									<p class="mb-0">Edit Profile</p>
									<button class="btn btn-primary btn-sm ms-auto" type="submit">Simpan</button>
								</div>
							</div>
							<div class="card-body">
								<p class="text-uppercase text-sm">Informasi Pengguna</p>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="example-text-input" class="form-control-label">Username</label>
											<input class="form-control" type="text" name="username"
												value="<?= $profile[0]['username']?>">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="example-text-input" class="form-control-label">Nama</label>
											<input class="form-control" type="text" name="nama"
												value="<?= $profile[0]['nama']?>">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="example-text-input" class="form-control-label">Ubah
												Password</label>
											<input class="form-control" type="text" name="password" placeholder="*****">
										</div>
									</div>
									<?php if ($this->session->userdata('id_role') == 3) { ?>
									<div class="col-md-6">
										<div class="form-group">
											<label for="example-text-input" class="form-control-label">NIK</label>
											<input class="form-control" type="number" name="nik"
												pattern="/^-?\d+\.?\d*$/"
												onKeyPress="if(this.value.length==16) return false;"
												value="<?= $profile[0]['nik']?>">
										</div>
									</div>
									<?php } ?>
									<div class="col-md-6">
										<div class="form-group">
											<label for="example-text-input" class="form-control-label">No. Telp</label>
											<input class="form-control" type="number" name="telp"
												value="<?= $profile[0]['telp']?>">
										</div>
									</div>
								</div>
								<hr class="horizontal dark">
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4">
				</div>
			</div>
			<footer class="footer pt-3  ">
				<div class="container-fluid">
					<div class="row align-items-center justify-content-lg-between">
						<div class="col-lg-6 mb-lg-0 mb-4">
							<div class="copyright text-center text-sm text-muted text-lg-start">
								© <script>
									document.write(new Date().getFullYear())

								</script>,
								made with <i class="fa fa-heart"></i> by
								<a href="#" class="font-weight-bold" target="_blank">Sandi</a>
								for a better web.
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modal-profile" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content text-center">
				<div class="modal-body" style="padding: 30px;">
					<!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
					<h2 class="mb-3 text-start" style="font-weight: 800; font-size: 24px;">
						Upload Foto Profil</h2>
					<form class="text-start" method="post" action="<?= base_url('Profile/uploadFoto'); ?>"
						enctype="multipart/form-data">
						<input type="file" class="form-control mt-4" id="foto" name="foto" required>
						<div class="d-flex justify-content-around">
							<button type="submit" class="btn btn-primary rounded-pill w-65 mt-4">Simpan</button>
							<button type="button" data-bs-dismiss="modal" aria-label="Close"
								class="btn btn-secondary rounded-pill w-25 mt-4">Batal</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- End Modal -->

	<!-- Modal -->
	<div class="modal fade" id="modal-laporan" tabindex="-1">
		<div class="modal-dialog modal-lg modal-dialog-centered">
			<div class="modal-content text-center">
				<div class="modal-body" style="padding: 30px;">
					<!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
					<h2 class="mb-3 text-start" style="font-weight: 800; font-size: 24px;">
						Laporan Saya</h2>
					<table class="table align-items-center mb-0">
						<thead>
							<tr>
								<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Isi Laporan</th>
								<th
									class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Foto</th>
								<th
									class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Tgl Pelaporan</th>
								<th
									class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Status</th>
							</tr>
						</thead>
						<tbody>
							<?php if (!empty($pengaduan)) : ?>
							<?php foreach ($pengaduan as $p) : ?>
							<tr>
								<td>
									<p class="text-xs font-weight-bold mb-0"><?= $p['judul_laporan']?></p>
									<p class="text-xs text-secondary mb-0"><?= $p['isi_laporan']?></p>
								</td>
								<td class="align-middle text-center">
									<div>
										<img src="<?= base_url('uploads/FileLaporan/'.$p['foto_laporan']) ?>"
											class="avatar" alt="foto_laporan">
									</div>
								</td>
								<!-- <td class="align-middle text-center">
											<span class="text-secondary text-xs font-weight-bold"><?= $p['tgl_kejadian']?></span>
										</td> -->
								<td class="align-middle text-center">
									<span
										class="text-secondary text-xs font-weight-bold"><?= $p['tgl_pengaduan']?></span>
								</td>
								<td class="align-middle text-center text-sm">
									<?php if ($p['status'] == 'proses') { ?>
									<span class="badge badge-sm bg-gradient-warning"><?= $p['status']?></span>
									<?php } else { ?>
									<span class="badge badge-sm bg-gradient-success"><?= $p['status']?></span>
									<?php } ?>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>

					<div class="d-flex justify-content-around">
						<button type="button" data-bs-dismiss="modal" aria-label="Close"
							class="btn btn-secondary rounded-pill w-100 mt-4">Batal</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Modal -->

	<style>
		.modal-laporan {
			width: 700px;
			margin: auto;
		}

	</style>

</body>

</html>
