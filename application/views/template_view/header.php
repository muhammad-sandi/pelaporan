<body>
	<div class="container-xxl bg-white p-0">
		<!-- Navbar & Hero Start -->
		<div class="container-xxl position-relative p-0">
			<nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
				<a href="<?= base_url()?>Home" class="navbar-brand p-0">
					<h1 class="m-0"><i class="fa fa-bullhorn me-2"></i>LAPOR<span class="fs-5">Online</span></h1>
					<!-- <img src="img/logo.png" alt="Logo"> -->
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
					<span class="fa fa-bars"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarCollapse">
					<div class="navbar-nav ms-auto py-0">
						<a href="#" class="nav-item nav-link">Home</a>
						<a href="#laporan" class="nav-item nav-link">Buat Laporan</a>
						<a href="#cara-laporan" class="nav-item nav-link">Cara Membuat Laporan</a>
						<a href="#laporan-publik" class="nav-item nav-link">Pelaporan Publik</a>
						<a href="#jumlah-laporan" class="nav-item nav-link">Jumlah Laporan</a>
					</div>
					<?php if (!empty($this->session->userdata('id_user'))) { ?>
					<a href="<?= base_url('Auth/logout')?>"
						class="btn btn-secondary text-light rounded-pill py-2 px-4 ms-3">Logout</a>
					<?php } else { ?>
					<div class="nav-item dropdown">
						<a href="#"
							class="nav-link dropdown-toggle btn btn-secondary text-light rounded-pill py-2 px-4 ms-3"
							data-bs-toggle="dropdown">Daftar</a>
						<div class="dropdown-menu m-0">
							<a href="<?= base_url('Auth/register')?>" class="dropdown-item">Masyarakat</a>
							<a href="<?= base_url('Auth/registerAdmin')?>" class="dropdown-item">Admin</a>
						</div>
					</div>
					<?php } ?>
					<!-- <a href=""
				class="btn btn-secondary text-light rounded-pill py-2 px-4 ms-3">Masuk</a> -->
				</div>
			</nav>
			<!-- Navbar & Hero End -->

			<!-- Spinner Start -->
			<div id="spinner"
				class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
				<div class="spinner-grow text-primary" style="width: 3rem; height: 3rem;" role="status">
					<span class="sr-only">Loading...</span>
				</div>
			</div>
			<!-- Spinner End -->

			
