<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Referensi');
		$this->load->model('M_Auth');
		$this->load->library('form_validation');
		// auth_menu();
	}

	private function load()
	{
		$page = array(
			"head" => $this->load->view('template_profile/head', false, true),
            // "header" => $this->load->view('template_profile/header', false, true),
            "main_js" => $this->load->view('template_profile/main_js', false, true),
            // "footer" => $this->load->view('template_profile/footer', false, true)
        );
        
        return $page;
	}

	public function index()
	{
		// $this->form_validation->set_rules('username', 'Username', 'trim|required');
		$path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('Auth/login', false, true)
        );
        $this->load->view('template_profile/default_template', $data);
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$user = $this->M_Auth->getUser($username);

		if($user){
			if($user->password == $password){
				$role = $this->M_Auth->getRole($user->id);
				$status = $this->M_Auth->getStatus($username);

				$sessiondata = array(
					'id_user' => $user->id,
					'nama' => $user->nama,
					'username' => $user->username,
					'id_role' => $role->id_role,
					'nik' => $user->nik,
					'telp' => $user->telp,
					'id_masyarakat' => $user->id_masyarakat,
					'id_petugas' => $user->id_petugas
				);
				
				$this->session->set_userdata($sessiondata);
				if($status->status_user == 'aktif'){

				if($this->session->userdata('id_role') == 1 || 
		   		   $this->session->userdata('id_role') == 2){
				redirect('Admin');
			} else {
				redirect('Home');
			}
		}else{
			$this->session->set_flashdata('pesan', '<p style="font-size: 12px;" class="text-danger mt-2"><i>*Akun belum diaktivasi!</i></p>');
			redirect('Auth');
		}

			}else{
				
				// $data['errors'] = 'Password Salah!';
				$this->session->set_flashdata('pesan', '<p style="font-size: 12px;" class="text-danger mt-2"><i>*Password yang anda masukkan salah!</i></p>');
				redirect('Auth');
			}
		}else{
			
			$this->session->set_flashdata('pesan', '<p style="font-size: 12px;" class="text-danger mt-2"><i>*Username tidak ditemukan!</i></p>');
			redirect('Auth');
		}
	}

	public function register()
	{
		$path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('Auth/register', false, true)
        );
        $this->load->view('template_profile/default_template', $data);
	}

	public function registerAdmin()
	{
		$path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('Auth/register_admin', false, true)
        );
        $this->load->view('template_profile/default_template', $data);
	}

	public function registration()
	{
    $username = $this->input->post('username');
    $checkUsername = $this->M_Referensi->getTableWhere('user', array('username' => $username));
    if ($checkUsername) {
        $this->session->set_flashdata('pesan', '<p style="font-size: 12px;" class="text-danger mt-2"><i>*Username sudah digunakan!</i></p>');
        redirect('Auth/register');
    } else {
        $dataUser = array(
            'nama' => $this->input->post('nama'),
            'username' => $username,
            'password' => md5($this->input->post('password')),
            'create_date' => date('Y-m-d H:i:s'),
            'foto' => 'default.jpg',
            'status_user' => 'aktif',
            'id_role' => 3
        );

        $insertUser = $this->M_Referensi->insertTable('user', $dataUser);

        $dataMasyarakat = array(
            'create_date' => date('Y-m-d H:i:s'),
            'id_user' => $insertUser
        );

        $insertMasyarakat = $this->M_Referensi->insertTable('masyarakat', $dataMasyarakat);

        $dataRole = array(
            'id_role' => 3,
            'id_user' => $insertUser,
        );

        $insertRole = $this->M_Referensi->insertTable('userrole', $dataRole);

        if($insertUser && $insertMasyarakat && $insertRole){
            redirect('Auth');
        }else{
            redirect('Auth');
        }
   	}
	}

	public function registrationAdmin()
	{
	$username = $this->input->post('username');
	$checkUsername = $this->M_Referensi->getTableWhere('user', array('username' => $username));
	if ($checkUsername) {
		$this->session->set_flashdata('pesan', '<p style="font-size: 12px;" class="text-danger mt-2"><i>*Username sudah digunakan!</i></p>');
		redirect('Auth/registerAdmin');
	} else {
		$dataUser = array(
			'nama' => $this->input->post('nama'),
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'create_date' => date('Y-m-d H:i:s'),
			'foto' => 'default.jpg',
			'status_user' => 'proses',
			'id_role' => 1
		);

		$insertUser = $this->M_Referensi->insertTable('user', $dataUser);

		$dataPetugas = array(
			'create_date' => date('Y-m-d H:i:s'),
			'id_role' => 1,
			'id_user' => $insertUser
		);

		$insertPetugas = $this->M_Referensi->insertTable('petugas', $dataPetugas);

		$dataRole = array(
			'id_role' => 1,
			'id_user' => $insertUser,
		);

		$insertRole = $this->M_Referensi->insertTable('userrole', $dataRole);

		if($insertUser && $insertPetugas && $insertRole){
			redirect('Auth');
		}else{
			redirect('Auth');
		}
	}
	}

	public function logout(){
		$this->session->sess_destroy();
		return redirect('Home');
	}
	
}
