<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Home');
		$this->load->model('M_Referensi');
		$this->load->model('M_Profile');
		// auth_menu();
	}

	private function load()
	{
		$page = array(
			"head" => $this->load->view('template_view/head', false, true),
            "header" => $this->load->view('template_view/header', false, true),
            "main_js" => $this->load->view('template_view/main_js', false, true),
            "footer" => $this->load->view('template_view/footer', false, true)
        );
        
        return $page;
	}

	public function index()
	{
		$data = array(
			'publik' => $this->M_Home->getDataPublik(),
			'profile' => $this->M_Profile->getDataProfile(),
			'jumlahlaporan' => $this->M_Home->countLaporan()
		);

		$path = "";
        $data = array(
            "page" => $this->load($path),
            "content" => $this->load->view('index', $data, true)
        );
        $this->load->view('template_view/default_template', $data);
	}

	public function isiLaporan()
	{
		$tgl = date('YmdHis');
			
		$config = array();
		$config['upload_path']          = './uploads/FileLaporan';
		$config['allowed_types']        = '*';
		$config['max_size']             = 10000;
			
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
	
		$_FILES['foto']['name'] = $tgl . '_' .$_FILES['foto']['name'];
		$_FILES['foto']['type'] = $_FILES['foto']['type'];
		$_FILES['foto']['tmp_name'] = $_FILES['foto']['tmp_name'];
		$_FILES['foto']['error'] = $_FILES['foto']['error'];
		$_FILES['foto']['size'] = $_FILES['foto']['size'];
	
		if ($this->upload->do_upload('foto')) 
		{
			$uploadData = $this->upload->data();
		}
		
		if($uploadData['file_name'] == NULL || $uploadData['file_name'] == ""){

			$data = array(
				'judul_laporan' => $this->input->post('judul'),
				'tgl_kejadian' => $this->input->post('tgl_kejadian'),
				'tgl_pengaduan' => date('Y-m-d H:i:s'),
				'foto' => 'default.jpg',
				'privasi' => $this->input->post('privasi'),
				'isi_laporan' => $this->input->post('isi'),
				'id_masyarakat' => $this->session->userdata('id_masyarakat'),
				'id_masyarakat' => $this->session->userdata('id_masyarakat'),
				'status' => 'proses'
	
			);
	
			$insert = $this->M_Referensi->insertTable('pengaduan', $data);
			
		} else {
	
			$data = array(
				'judul_laporan' => $this->input->post('judul'),
				'tgl_kejadian' => $this->input->post('tgl_kejadian'),
				'tgl_pengaduan' => date('Y-m-d H:i:s'),
				'foto' => $uploadData['file_name'],
				'privasi' => $this->input->post('privasi'),
				'isi_laporan' => $this->input->post('isi'),
				'id_masyarakat' => $this->session->userdata('id_masyarakat'),
				'id_masyarakat' => $this->session->userdata('id_masyarakat'),
				'status' => 'proses'
	
			);
	
			$insert = $this->M_Referensi->insertTable('pengaduan', $data);

		}

		if($insert){
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

}
