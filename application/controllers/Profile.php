<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Auth');
		$this->load->model('M_Profile');
		// auth_menu();
	}

	private function load()
	{
		$page = array(
			"head" => $this->load->view('template_profile/head', false, true),
            "main_js" => $this->load->view('template_profile/main_js', false, true),
        );
        
        return $page;
	}

	public function index()
	{
		if($this->session->userdata('id_user')){

			$id = $this->session->userdata('id_masyarakat');

			$data = array(
				'profile' => $this->M_Profile->getDataProfile(),
				'pengaduan' => $this->M_Profile->getDataPengaduanId($id)
			);
	
			$path = "";
			$data = array(
				"page" => $this->load($path),
				"content" => $this->load->view('Profile/profile', $data, true)
			);
			$this->load->view('template_profile/default_template', $data);
		} else {
			redirect('Home');
		}
	}

	public function updateUser(){

		if($this->session->userdata('id_role') == 3){

        if($this->input->post('password') != null || $this->input->post('password') != ""){
		$dataUser = array(
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'nama' => $this->input->post('nama'),
			'nik' => $this->input->post('nik'),
			'telp' => $this->input->post('telp')
		);
        } else {
            $dataUser = array(
                'username' => $this->input->post('username'),
				'nama' => $this->input->post('nama'),
				'nik' => $this->input->post('nik'),
				'telp' => $this->input->post('telp')
        );
        }

		$this->db->where('id', $this->input->post('id_user'));
		$updateUser = $this->db->update('user', $dataUser);

		$dataMasyarakat = array(
			'nik' => $this->input->post('nik'),
			'telp' => $this->input->post('telp'),
		);

		$this->db->where('id_user', $this->input->post('id_user'));
		$updateMasyarakat = $this->db->update('masyarakat', $dataMasyarakat);

		if($updateUser || $updateMasyarakat){
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}

		}

		if($this->session->userdata('id_role') == 1 || 2){

			if($this->input->post('password') != null || $this->input->post('password') != ""){
			$dataUser = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'nama' => $this->input->post('nama'),
				'telp' => $this->input->post('telp')
			);
			} else {
				$dataUser = array(
					'username' => $this->input->post('username'),
					'nama' => $this->input->post('nama'),
					'telp' => $this->input->post('telp')
			);
			}
	
			$this->db->where('id', $this->input->post('id_user'));
			$updateUser = $this->db->update('user', $dataUser);
	
			$dataPetugas = array(
				'telp' => $this->input->post('telp'),
			);
	
			$this->db->where('id_user', $this->input->post('id_user'));
			$updatePetugas = $this->db->update('petugas', $dataPetugas);

			if($updateUser || $updatePetugas){
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
			
			}
		
	}

	public function uploadFoto()
	{
		$tgl = date('YmdHis');

		$config = array();
		$config['upload_path']          = './uploads/FileProfile';
		$config['allowed_types']        = '*';
		$config['max_size']             = 10000;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$_FILES['foto']['name'] = $tgl . '_' .$_FILES['foto']['name'];
		$_FILES['foto']['type'] = $_FILES['foto']['type'];
		$_FILES['foto']['tmp_name'] = $_FILES['foto']['tmp_name'];
		$_FILES['foto']['error'] = $_FILES['foto']['error'];
		$_FILES['foto']['size'] = $_FILES['foto']['size'];

		if ($this->upload->do_upload('foto')) 
    	{
			$uploadData = $this->upload->data();
   		}

		$data = array(
			'foto' => $uploadData['file_name'],
		);

		$this->db->where('id', $this->session->userdata('id_user'));
		$update = $this->db->update('user', $data);

		if($update){
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

}
