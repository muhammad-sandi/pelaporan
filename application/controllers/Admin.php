<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Auth');
		$this->load->model('M_Admin');
		$this->load->model('M_Profile');
		$this->load->model('M_Referensi');
		$this->load->library('pagination');
		// auth_menu();
	}

	private function load()
	{
		$page = array(
			"head" => $this->load->view('template_admin/head', false, true),
			"sidebar" => $this->load->view('template_admin/sidebar', false, true),
            "footer" => $this->load->view('template_admin/footer', false, true),
            "main_js" => $this->load->view('template_admin/main_js', false, true),
        );
        
        return $page;
	}

	public function index()
	{
		if($this->session->userdata('id_role') == 1 || 
		   $this->session->userdata('id_role') == 2){

			$data = array(
				'profile' => $this->M_Profile->getDataProfile(),
				'semua_pengaduan' => $this->M_Admin->countSemuaPengaduan(),
				'pengaduan_diproses' => $this->M_Admin->countPengaduanDiproses(),
				'pengaduan_selesai' => $this->M_Admin->countPengaduanSelesai(),
				'masyarakat' => $this->M_Admin->countMasyarakat(),
				'pengaduan' => $this->M_Admin->getDataPengaduanTerbaru(),
			);
	
			$path = "";
			$data = array(
				"page" => $this->load($path),
				"content" => $this->load->view('Admin/dashboard', $data, true)
			);
			$this->load->view('template_admin/default_template', $data);
		} else {
			redirect('Home');
		}
	}

	public function masyarakat($page = 1)
	{
    	if ($this->session->userdata('id_role') == 1 || $this->session->userdata('id_role') == 2) {

		// Menyimpan data filter pada session
		if (!empty($this->input->post('keyword'))) {
		    $this->session->set_userdata('filter_keyword', $this->input->post('keyword'));
		}
		
		// Membaca data filter dari session
		$keyword = $this->session->userdata('filter_keyword');
		
		if(empty($keyword) || $keyword == 'semua'){
		    $keyword = null;
		}

        	$limit_per_page = 10;
        	$total_records = $this->M_Admin->countMasyarakatSearch($keyword);
        	$config['base_url'] = base_url('Admin/masyarakat/');
        	$config['total_rows'] = $total_records;
        	$config['per_page'] = $limit_per_page;
        	$config['uri_segment'] = 3;
        	$config['use_page_numbers'] = TRUE;
        	$config['reuse_query_string'] = TRUE;
        	$this->pagination->initialize($config);

        	$offset = ($page - 1) * $limit_per_page;

        	$data = array(
            	'masyarakat' => $this->M_Admin->getDataMasyarakat($keyword, $limit_per_page, $offset),
            	'profile' => $this->M_Profile->getDataProfile(),
				'keyword' => $keyword,
			  	'pagination' => $this->pagination->create_links() . http_build_query($this->uri->uri_to_assoc(3))
        	);

        	$path = "";
        	$data = array(
            	"page" => $this->load($path),
            	"content" => $this->load->view('Admin/masyarakat', $data, true)
        	);
        	$this->load->view('template_admin/default_template', $data);
    	} else {
        	redirect('Home');
    	}
	}

	public function semua_pengaduan($page = 1){
		if ($this->session->userdata('id_role') == 1 || $this->session->userdata('id_role') == 2) {
		
		// Menyimpan data filter pada session
		if (!empty($this->input->post('keyword'))) {
		    $this->session->set_userdata('filter_keyword', $this->input->post('keyword'));
		}
		if (!empty($this->input->post('bulan'))) {
		    $this->session->set_userdata('filter_bulan', $this->input->post('bulan'));
		}
		
		// Membaca data filter dari session
		$keyword = $this->session->userdata('filter_keyword');
		$bulan = $this->session->userdata('filter_bulan');
		
		// Tambahkan kondisi jika memilih Semua Bulan'
		if(empty($bulan) || $bulan == 'semua'){
		    $bulan = null;
		}
		
		if(empty($keyword) || $keyword == 'semua'){
		    $keyword = null;
		}
	  
		    $limit_per_page = 10;
		    $total_records = $this->M_Admin->countSemuaPengaduanSearch($keyword, $bulan);
		    $config['base_url'] = base_url('Admin/semua_pengaduan/');
		    $config['total_rows'] = $total_records;
		    $config['per_page'] = $limit_per_page;
		    $config['uri_segment'] = 3;
		    $config['use_page_numbers'] = TRUE;
		    $config['reuse_query_string'] = TRUE;
		    $this->pagination->initialize($config);
	  
		    $offset = ($page - 1) * $limit_per_page;
	  
		    $data = array(
			  'pengaduan' => $this->M_Admin->getDataPengaduan($keyword, $limit_per_page, $offset, $bulan),
			  'profile' => $this->M_Profile->getDataProfile(),
			  'keyword' => $keyword,
			  'bulan' => $bulan,
			  'pagination' => $this->pagination->create_links() . http_build_query($this->uri->uri_to_assoc(3))
		    );
	
			$path = "";
			$data = array(
				"page" => $this->load($path),
				"content" => $this->load->view('Admin/semua_pengaduan', $data, true)
			);
			$this->load->view('template_admin/default_template', $data);
		} else {
			redirect('Home');
		}
	}

	public function pengaduan_diproses($page = 1)
	{
		if($this->session->userdata('id_role') == 1 || $this->session->userdata('id_role') == 2){

		// Menyimpan data filter pada session
		if (!empty($this->input->post('keyword'))) {
		    $this->session->set_userdata('filter_keyword', $this->input->post('keyword'));
		}
		if (!empty($this->input->post('bulan'))) {
		    $this->session->set_userdata('filter_bulan', $this->input->post('bulan'));
		}
		
		// Membaca data filter dari session
		$keyword = $this->session->userdata('filter_keyword');
		$bulan = $this->session->userdata('filter_bulan');
		
		// Tambahkan kondisi jika memilih Semua Bulan'
		if(empty($bulan) || $bulan == 'semua'){
		    $bulan = null;
		}
		
		if(empty($keyword) || $keyword == 'semua'){
		    $keyword = null;
		}
			
			$limit_per_page = 10;
        	$total_records = $this->M_Admin->countPengaduanDiprosesSearch($keyword, $bulan);
        	$config['base_url'] = base_url('Admin/pengaduan_diproses/');
        	$config['total_rows'] = $total_records;
        	$config['per_page'] = $limit_per_page;
        	$config['uri_segment'] = 3;
        	$config['use_page_numbers'] = TRUE;
        	$config['reuse_query_string'] = TRUE;
        	$this->pagination->initialize($config);  

        	$offset = ($page - 1) * $limit_per_page;

			$data = array(
				'pengaduan' => $this->M_Admin->getDataPengaduanProses($keyword, $limit_per_page, $offset, $bulan),
				'profile' => $this->M_Profile->getDataProfile(),
				'keyword' => $keyword,
				'bulan' => $bulan,
				'pagination' => $this->pagination->create_links() . http_build_query($this->uri->uri_to_assoc(3))
			);
	
			$path = "";
			$data = array(
				"page" => $this->load($path),
				"content" => $this->load->view('Admin/pengaduan_diproses', $data, true)
			);
			$this->load->view('template_admin/default_template', $data);
		} else {
			redirect('Home');
		}
	}

	public function pengaduan_selesai($page = 1)
	{
		if($this->session->userdata('id_role') == 1 || $this->session->userdata('id_role') == 2){

		// Menyimpan data filter pada session
		if (!empty($this->input->post('keyword'))) {
		    $this->session->set_userdata('filter_keyword', $this->input->post('keyword'));
		}
		if (!empty($this->input->post('bulan'))) {
		    $this->session->set_userdata('filter_bulan', $this->input->post('bulan'));
		}
		
		// Membaca data filter dari session
		$keyword = $this->session->userdata('filter_keyword');
		$bulan = $this->session->userdata('filter_bulan');
		
		// Tambahkan kondisi jika memilih Semua Bulan'
		if(empty($bulan) || $bulan == 'semua'){
		    $bulan = null;
		}
		
		if(empty($keyword) || $keyword == 'semua'){
		    $keyword = null;
		}

			$limit_per_page = 10;
        	$total_records = $this->M_Admin->countPengaduanSelesaiSearch($keyword, $bulan);
        	$config['base_url'] = base_url('Admin/pengaduan_selesai/');
        	$config['total_rows'] = $total_records;
        	$config['per_page'] = $limit_per_page;
        	$config['uri_segment'] = 3;
        	$config['use_page_numbers'] = TRUE;
        	$config['reuse_query_string'] = TRUE;
        	$this->pagination->initialize($config);

        	$offset = ($page - 1) * $limit_per_page;

			$data = array(
				'pengaduan' => $this->M_Admin->getDataPengaduanSelesai($keyword, $limit_per_page, $offset, $bulan),
				'profile' => $this->M_Profile->getDataProfile(),
				'keyword' => $keyword,
				'bulan' => $bulan,
				'pagination' => $this->pagination->create_links() . http_build_query($this->uri->uri_to_assoc(3))
			);
	
			$path = "";
			$data = array(
				"page" => $this->load($path),
				"content" => $this->load->view('Admin/pengaduan_selesai', $data, true)
			);
			$this->load->view('template_admin/default_template', $data);
		} else {
			redirect('Home');
		}
	}

	public function tanggapan($page = 1)
	{
		if($this->session->userdata('id_role') == 1 || $this->session->userdata('id_role') == 2){

		// Menyimpan data filter pada session
		if (!empty($this->input->post('keyword'))) {
		    $this->session->set_userdata('filter_keyword', $this->input->post('keyword'));
		}
		
		// Membaca data filter dari session
		$keyword = $this->session->userdata('filter_keyword');
		
		if(empty($keyword) || $keyword == 'semua'){
		    $keyword = null;
		}

			$limit_per_page = 10;
        	$total_records = $this->M_Admin->countTanggapanSearch($keyword);
        	$config['base_url'] = base_url('Admin/tanggapan/');
        	$config['total_rows'] = $total_records;
        	$config['per_page'] = $limit_per_page;
        	$config['uri_segment'] = 3;
        	$config['use_page_numbers'] = TRUE;
        	$config['reuse_query_string'] = TRUE;
        	$this->pagination->initialize($config);

        	$offset = ($page - 1) * $limit_per_page;

			$data = array(
				'tanggapan' => $this->M_Admin->getDataTanggapan($keyword, $limit_per_page, $offset),
				'profile' => $this->M_Profile->getDataProfile(),
				'keyword' => $keyword,
			  	'pagination' => $this->pagination->create_links() . http_build_query($this->uri->uri_to_assoc(3))
			);
	
			$path = "";
			$data = array(
				"page" => $this->load($path),
				"content" => $this->load->view('Admin/tanggapan', $data, true)
			);
			$this->load->view('template_admin/default_template', $data);
		} else {
			redirect('Home');
		}
	}

	public function terimaPengaduan()
	{
		$dataTanggapan = array(
			'id_pengaduan' => $this->input->post('id_pengaduan'),
			'tgl_tanggapan' => date('Y-m-d H:i:s'),
			'tanggapan' => $this->input->post('tanggapan'),
			'id_petugas' => $this->input->post('id_petugas'),
		);

		$insertTanggapan = $this->M_Referensi->insertTable('tanggapan', $dataTanggapan);

		$dataPengaduan = array(
			'status' => 'selesai',
		);

		$this->db->where('id', $this->input->post('id_pengaduan'));
		$updatePengaduan = $this->db->update('pengaduan', $dataPengaduan);

		if($insertTanggapan || $updatePengaduan){
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function tolakPengaduan($id){
        $id = array('id' => $id);
        $this->M_Referensi->deleteTable($id,'pengaduan');
        
        redirect($_SERVER['HTTP_REFERER']);
    }

	public function petugas($page = 1)
	{
		if($this->session->userdata('id_role') == 1 || $this->session->userdata('id_role') == 2){

		// Menyimpan data filter pada session
		if (!empty($this->input->post('keyword'))) {
		    $this->session->set_userdata('filter_keyword', $this->input->post('keyword'));
		}
		
		// Membaca data filter dari session
		$keyword = $this->session->userdata('filter_keyword');
		
		if(empty($keyword) || $keyword == 'semua'){
		    $keyword = null;
		}

			$limit_per_page = 10;
        	$total_records = $this->M_Admin->countPetugasSearch($keyword);
        	$config['base_url'] = base_url('Admin/petugas/');
        	$config['total_rows'] = $total_records;
        	$config['per_page'] = $limit_per_page;
        	$config['uri_segment'] = 3;
        	$config['use_page_numbers'] = TRUE;
        	$config['reuse_query_string'] = TRUE;
        	$this->pagination->initialize($config);

        	$offset = ($page - 1) * $limit_per_page;

			$data = array(
				'profile' => $this->M_Profile->getDataProfile(),
				'petugas' => $this->M_Admin->getDataPetugas($keyword, $limit_per_page, $offset),
				'keyword' => $keyword,
			  	'pagination' => $this->pagination->create_links() . http_build_query($this->uri->uri_to_assoc(3))
			);
	
			$path = "";
			$data = array(
				"page" => $this->load($path),
				"content" => $this->load->view('Admin/petugas', $data, true)
			);
			$this->load->view('template_admin/default_template', $data);
		} else {
			redirect('Home');
		}
	}

	public function tambahPetugas()
	{
	$username = $this->input->post('username');
	$checkUsername = $this->M_Referensi->getTableWhere('user', array('username' => $username));
	if ($checkUsername) {
		$this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show text-white" role="alert">
		<strong>Mohon maaf</strong>, username telah digunakkan.
		<button type="button" class="btn-close fa fa-times" data-bs-dismiss="alert" aria-label="Close"></button>
	  	</div>');
		redirect('Admin/petugas');
	} else {
		$dataUser = array(
			'nama' => $this->input->post('nama'),
			'telp' => $this->input->post('telp'),
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'create_date' => date('Y-m-d H:i:s'),
			'foto' => 'default.jpg',
			'status_user' => 'aktif',
			'id_role' => 2
		);

		$insertUser = $this->M_Referensi->insertTable('user', $dataUser);

		$dataPetugas = array(
			'telp' => $this->input->post('telp'),
			'create_date' => date('Y-m-d H:i:s'),
			'id_role' => 2,
			'id_user' => $insertUser
		);

		$insertPetugas = $this->M_Referensi->insertTable('petugas', $dataPetugas);

		$dataRole = array(
			'id_role' => 2,
			'id_user' => $insertUser,
		);

		$insertRole = $this->M_Referensi->insertTable('userrole', $dataRole);

		if($insertUser && $insertPetugas && $insertRole){
		$this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show text-white" role="alert">
		<strong>Selamat!</strong> data petugas berhasil ditambahkan.
		<button type="button" class="btn-close fa fa-times" data-bs-dismiss="alert" aria-label="Close"></button>
	  	</div>');
		redirect('Admin/petugas');
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	}

	public function aktivasiUser($id){
		$data = array(
			'status_user' => 'aktif'
		);

		$this->db->where('id', $id);
		$update = $this->db->update('user', $data);

		redirect($_SERVER['HTTP_REFERER']);
	}

	function hapusUser($id){
        $id = array('id' => $id);
        $this->M_Referensi->deleteTable($id,'user');
        
        redirect($_SERVER['HTTP_REFERER']);
    }

}
