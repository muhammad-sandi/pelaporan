<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;

class Export extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Referensi');
		$this->load->model('M_Admin');
	}

	public function excel_masyarakat($keyword = NULL)
	{
        $data = array(
			'title' => 'Data Masyarakat',
			'data' => $this->M_Admin->getDataMasyarakat(urldecode($keyword), NULL, NULL, NULL)
		);

        $this->load->view('Export/excel_masyarakat', $data);
	}
	
	public function excel_semua_pengaduan_search($keyword = NULL, $bulan = NULL)
	{
        $data = array(
			'title' => 'Semua Pengaduan',
			'data' => $this->M_Admin->getDataPengaduan(urldecode($keyword), NULL, NULL, $bulan)
		);

        $this->load->view('Export/excel_semua_pengaduan', $data);
	}

	public function excel_semua_pengaduan($bulan = NULL)
	{
        $data = array(
			'title' => 'Semua Pengaduan',
			'data' => $this->M_Admin->getDataPengaduan(NULL, NULL, NULL, $bulan)
		);

        $this->load->view('Export/excel_semua_pengaduan', $data);
	}

	public function excel_pengaduan_diproses_search($keyword = NULL, $bulan = NULL)
	{
        $data = array(
			'title' => 'Pengaduan Diproses',
			'data' => $this->M_Admin->getDataPengaduanProses(urldecode($keyword), NULL, NULL, NULL)
		);

        $this->load->view('Export/excel_pengaduan_diproses', $data);
	}

	public function excel_pengaduan_diproses($bulan = NULL)
	{
        $data = array(
			'title' => 'Pengaduan Diproses',
			'data' => $this->M_Admin->getDataPengaduanProses(NULL, NULL, NULL, $bulan)
		);

        $this->load->view('Export/excel_pengaduan_diproses', $data);
	}

	public function excel_pengaduan_selesai_search($keyword = NULL, $bulan = NULL)
	{
        $data = array(
			'title' => 'Pengaduan Selesai',
			'data' => $this->M_Admin->getDataPengaduanSelesai(urldecode($keyword), NULL, NULL, NULL)
		);

        $this->load->view('Export/excel_pengaduan_selesai', $data);
	}

	public function excel_pengaduan_selesai($bulan = NULL)
	{
        $data = array(
			'title' => 'Pengaduan Selesai',
			'data' => $this->M_Admin->getDataPengaduanSelesai(NULL, NULL, NULL, $bulan)
		);

        $this->load->view('Export/excel_pengaduan_selesai', $data);
	}

	public function excel_tanggapan($keyword = NULL)
	{
        $data = array(
			'title' => 'Data Tanggapan',
			'data' => $this->M_Admin->getDataTanggapan(urldecode($keyword), NULL, NULL, NULL)
		);

        $this->load->view('Export/excel_tanggapan', $data);
	}

	public function excel_petugas($keyword = NULL)
	{
        $data = array(
			'title' => 'Data Petugas',
			'data' => $this->M_Admin->getDataPetugas(urldecode($keyword), NULL, NULL, NULL)
		);

        $this->load->view('Export/excel_petugas', $data);
	}

	public function pdf_masyarakat($keyword = NULL)
    {
        $data = $this->M_Admin->getDataMasyarakat(urldecode($keyword), NULL, NULL, NULL);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_masyarakat', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }

	public function pdf_semua_pengaduan_search($keyword = NULL, $bulan = NULL)
    {
        $data = $this->M_Admin->getDataPengaduan(urldecode($keyword), NULL, NULL, $bulan);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_semua_pengaduan', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }

	public function pdf_semua_pengaduan($bulan = NULL)
    {
        $data = $this->M_Admin->getDataPengaduan(NULL, NULL, NULL, $bulan);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_semua_pengaduan', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }

	public function pdf_pengaduan_diproses_search($keyword = NULL, $bulan = NULL)
    {
        $data = $this->M_Admin->getDataPengaduanProses(urldecode($keyword), NULL, NULL, $bulan);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_pengaduan_diproses', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }

	public function pdf_pengaduan_diproses($bulan = NULL)
    {
        $data = $this->M_Admin->getDataPengaduanProses(NULL, NULL, NULL, $bulan);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_pengaduan_diproses', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }

	public function pdf_pengaduan_selesai_search($keyword = NULL, $bulan = NULL)
    {
        $data = $this->M_Admin->getDataPengaduanSelesai(urldecode($keyword), NULL, NULL, $bulan);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_pengaduan_selesai', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }

	public function pdf_pengaduan_selesai($bulan = NULL)
    {
        $data = $this->M_Admin->getDataPengaduanSelesai(NULL, NULL, NULL, $bulan);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_pengaduan_selesai', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }

	public function pdf_tanggapan($keyword = NULL)
    {
        $data = $this->M_Admin->getDataTanggapan(urldecode($keyword), NULL, NULL, NULL);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_tanggapan', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }

	public function pdf_petugas($keyword = NULL)
    {
        $data = $this->M_Admin->getDataPetugas(urldecode($keyword), NULL, NULL, NULL);

        require_once APPPATH.'third_party/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();

        $html = $this->load->view('Export/pdf_petugas', array('data' => $data), true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $dompdf->stream("data.pdf", array("Attachment" => false));
    }
}
