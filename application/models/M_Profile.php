<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Profile extends CI_Model {

	public function getDataProfile(){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id', $this->session->userdata('id_user'));
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function getDataPengaduanId($id){
		$this->db->select('p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_kejadian, "%d %M %Y") as tgl_kejadian, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');
		$this->db->where('id_masyarakat', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	

}
