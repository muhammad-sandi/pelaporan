<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Referensi extends CI_Model {

	public function insertTable($table,$data){
		$query = $this->db->insert($table, $data);
		$id = $this->db->insert_id();

		return $id;
	}

	function deleteTable($id,$table){
		$this->db->where($id);
		$this->db->delete($table);
	}

	public function getTableWhere($table, $where)
    {
        $query = $this->db->get_where($table, $where);
        return $query->row_array();
    }
	
}
