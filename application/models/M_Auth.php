<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Auth extends CI_Model {

	public function getUser($username){
		$this->db->select('user.*, masyarakat.id as id_masyarakat, petugas.id as id_petugas');
		$this->db->from('user');
		$this->db->join('masyarakat', 'masyarakat.id_user = user.id', 'left');
		$this->db->join('petugas', 'petugas.id_user = user.id', 'left');
		$this->db->where('username', $username);
		$query = $this->db->get()->row();
		return $query;
	}

	public function getStatus($username){
		$this->db->select('status_user');
		$this->db->from('user');
		$this->db->where('username', $username);
		$query = $this->db->get()->row();
		return $query;
	}

	public function getRole($id){
		$this->db->select('*');
		$this->db->from('userrole');
		$this->db->where('id_user', $id);
		$query = $this->db->get()->row();
		return $query;
	}

	public function getMasyarakat($id){
		$this->db->select('*');
		$this->db->from('masyarakat');
		$this->db->where('id', $id);
		$query = $this->db->get()->row();
		return $query;
	}

}
