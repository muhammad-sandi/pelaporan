<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Home extends CI_Model {

	public function getDataPublik(){
		$this->db->select('p.isi_laporan, p.id_masyarakat, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');
		$this->db->where('privasi', 'publik');
		$this->db->where('status', 'selesai');
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function countLaporan()
	{
		$this->db->select('*');
		$this->db->from('pengaduan');
		$query = $this->db->get()->num_rows();
		return $query;
	}

}
