<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin extends CI_Model {

	public function getDataMasyarakat($keyword, $limit, $offset){
		$this->db->select('*');
		$this->db->from('masyarakat m');
		$this->db->join('user u', 'u.id = m.id_user', 'left');

		$this->db->group_start();
		$this->db->like('nama', $keyword);
		$this->db->or_like('u.username', $keyword);
		$this->db->or_like('u.nik', $keyword);
		$this->db->or_like('u.telp', $keyword);
		$this->db->group_end();

		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function getDataPengaduanTerbaru(){
		$this->db->select('p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_kejadian, "%d %M %Y") as tgl_kejadian, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');
		$this->db->order_by('p.id', 'desc');
		$this->db->limit(5);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function getDataPengaduan($keyword, $limit, $offset, $bulan){
		$this->db->select('p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_kejadian, "%d %M %Y") as tgl_kejadian, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');
		
		// tambahkan kondisi jika bulan tidak dipilih, tampilkan semua data
		if(is_null($bulan) || ($bulan == '')){
			$this->db->where('MONTH(tgl_pengaduan) != ""');
		} else {
			$this->db->where('MONTH(tgl_pengaduan)', $bulan);
		}
		
		$this->db->group_start();
		$this->db->like('judul_laporan', $keyword);
		$this->db->or_like('isi_laporan', $keyword);
		$this->db->or_like('nama', $keyword);
		$this->db->or_like('username', $keyword);
		$this->db->or_like('status', $keyword);
		$this->db->group_end();
		
		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function getDataPengaduanProses($keyword, $limit, $offset, $bulan){
		$this->db->select('p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_kejadian, "%d %M %Y") as tgl_kejadian, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');
		$this->db->where('status', 'proses');

		// tambahkan kondisi jika bulan tidak dipilih, tampilkan semua data
		if(is_null($bulan) || ($bulan == '')){
			$this->db->where('MONTH(tgl_pengaduan) != ""');
		} else {
			$this->db->where('MONTH(tgl_pengaduan)', $bulan);
		}

		$this->db->group_start();
		$this->db->like('judul_laporan', $keyword);
		$this->db->or_like('isi_laporan', $keyword);
		$this->db->or_like('nama', $keyword);
		$this->db->or_like('username', $keyword);
		$this->db->or_like('status', $keyword);
		$this->db->group_end();

		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function getDataPengaduanSelesai($keyword, $limit, $offset, $bulan){
		$this->db->select('p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_kejadian, "%d %M %Y") as tgl_kejadian, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');
		$this->db->where('status', 'selesai');

		// tambahkan kondisi jika bulan tidak dipilih, tampilkan semua data
		if(is_null($bulan) || ($bulan == '')){
			$this->db->where('MONTH(tgl_pengaduan) != ""');
		} else {
			$this->db->where('MONTH(tgl_pengaduan)', $bulan);
		}

		$this->db->group_start();
		$this->db->like('judul_laporan', $keyword);
		$this->db->or_like('isi_laporan', $keyword);
		$this->db->or_like('nama', $keyword);
		$this->db->or_like('username', $keyword);
		$this->db->or_like('status', $keyword);
		$this->db->group_end();

		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function getDataTanggapan($keyword, $limit, $offset){
		$this->db->select('t.*, DATE_FORMAT(tgl_tanggapan, "%d %M %Y") as tgl_tanggapan, pt.*, p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.foto, u.username');
		$this->db->from('tanggapan t');
		$this->db->join('pengaduan p', 'p.id = t.id_pengaduan', 'left');
		$this->db->join('petugas pt', 'pt.id = t.id_petugas', 'left');
		$this->db->join('user u', 'u.id = pt.id_user', 'left');

		$this->db->group_start();
		$this->db->like('judul_laporan', $keyword);
		$this->db->or_like('isi_laporan', $keyword);
		$this->db->or_like('nama', $keyword);
		$this->db->or_like('username', $keyword);
		$this->db->or_like('tanggapan', $keyword);
		$this->db->group_end();

		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function getDataPetugas($keyword, $limit, $offset){
		$this->db->select('*');
		$this->db->from('petugas p');
		$this->db->join('user u', 'u.id = p.id_user', 'left');
		$this->db->join('role r', 'r.id = p.id_role', 'left');

		$this->db->group_start();
		$this->db->like('nama', $keyword);
		$this->db->or_like('u.username', $keyword);
		$this->db->or_like('u.telp', $keyword);
		$this->db->or_like('u.status_user', $keyword);
		$this->db->or_like('r.role', $keyword);
		$this->db->group_end();

		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	public function countSemuaPengaduan(){
		$this->db->select('*');
		$this->db->from('pengaduan');
		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countSemuaPengaduanSearch($keyword, $bulan){
		$this->db->select('p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_kejadian, "%d %M %Y") as tgl_kejadian, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');

		// Filter berdasarkan bulan dan tahun jika telah diberikan parameter
		if ($bulan) {
			$this->db->where("MONTH(p.tgl_pengaduan) = $bulan");
			// $this->db->where("YEAR(p.tgl_pengaduan) = $tahun");
		}

		$this->db->group_start();
		$this->db->like('nama', $keyword);
		$this->db->or_like('u.username', $keyword);
		$this->db->or_like('u.telp', $keyword);
		$this->db->group_end();

		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countPengaduanDiproses(){
		$this->db->select('*');
		$this->db->from('pengaduan');
		$this->db->where('status', 'proses');
		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countPengaduanDiprosesSearch($keyword, $bulan){
		$this->db->select('p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_kejadian, "%d %M %Y") as tgl_kejadian, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');
		$this->db->where('status', 'proses');

		// Filter berdasarkan bulan dan tahun jika telah diberikan parameter
		if ($bulan) {
			$this->db->where("MONTH(p.tgl_pengaduan) = $bulan");
			// $this->db->where("YEAR(p.tgl_pengaduan) = $tahun");
		}

		$this->db->group_start();
		$this->db->like('nama', $keyword);
		$this->db->or_like('u.username', $keyword);
		$this->db->or_like('u.telp', $keyword);
		$this->db->group_end();

		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countPengaduanSelesai(){
		$this->db->select('*');
		$this->db->from('pengaduan');
		$this->db->where('status', 'selesai');
		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countPengaduanSelesaiSearch($keyword, $bulan){
		$this->db->select('p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_kejadian, "%d %M %Y") as tgl_kejadian, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.username, u.foto');
		$this->db->from('pengaduan p');
		$this->db->join('masyarakat m', 'p.id_masyarakat = m.id', 'left');
		$this->db->join('user u', 'u.id = m.id_user', 'left');
		$this->db->where('status', 'selesai');

		// Filter berdasarkan bulan dan tahun jika telah diberikan parameter
		if ($bulan) {
			$this->db->where("MONTH(p.tgl_pengaduan) = $bulan");
			// $this->db->where("YEAR(p.tgl_pengaduan) = $tahun");
		}

		$this->db->group_start();
		$this->db->like('nama', $keyword);
		$this->db->or_like('u.username', $keyword);
		$this->db->or_like('u.telp', $keyword);
		$this->db->group_end();

		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countMasyarakat(){
		$this->db->select('*');
		$this->db->from('masyarakat m');
		
		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countMasyarakatSearch($keyword){
		$this->db->select('*');
		$this->db->from('masyarakat m');
		$this->db->join('user u', 'u.id = m.id_user', 'left');

		$this->db->group_start();
		$this->db->like('nama', $keyword);
		$this->db->or_like('u.username', $keyword);
		$this->db->or_like('u.telp', $keyword);
		$this->db->group_end();
		
		$query = $this->db->get()->num_rows();
		return $query;
	}
	
	public function countTanggapan(){
		$this->db->select('*');
		$this->db->from('tanggapan');
		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countTanggapanSearch($keyword){
		$this->db->select('t.*, DATE_FORMAT(tgl_tanggapan, "%d %M %Y") as tgl_tanggapan, pt.*, p.*, p.foto as foto_laporan, DATE_FORMAT(tgl_pengaduan, "%d %M %Y") as tgl_pengaduan, u.nama, u.foto, u.username');
		$this->db->from('tanggapan t');
		$this->db->join('pengaduan p', 'p.id = t.id_pengaduan', 'left');
		$this->db->join('petugas pt', 'pt.id = t.id_petugas', 'left');
		$this->db->join('user u', 'u.id = pt.id_user', 'left');

		$this->db->group_start();
		$this->db->like('judul_laporan', $keyword);
		$this->db->or_like('isi_laporan', $keyword);
		$this->db->or_like('nama', $keyword);
		$this->db->or_like('username', $keyword);
		$this->db->or_like('tanggapan', $keyword);
		$this->db->group_end();

		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countPetugas(){
		$this->db->select('*');
		$this->db->from('petugas');
		$query = $this->db->get()->num_rows();
		return $query;
	}

	public function countPetugasSearch($keyword){
		$this->db->select('*');
		$this->db->from('petugas p');
		$this->db->join('user u', 'u.id = p.id_user', 'left');
		$this->db->join('role r', 'r.id = p.id_role', 'left');

		$this->db->group_start();
		$this->db->like('nama', $keyword);
		$this->db->or_like('u.username', $keyword);
		$this->db->or_like('u.telp', $keyword);
		$this->db->group_end();

		$query = $this->db->get()->num_rows();
		return $query;
	}
}
